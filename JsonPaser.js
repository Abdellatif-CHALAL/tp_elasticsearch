var fs = require('file-system');

var Document = fs.readFileSync('data.csv').toString().split('\r\n');
var Columns = Document[0];
Document.shift()
Columns = Columns.split(';')
var Json = []

for (var i = 0; i < Document.length; i++) {
  var Data = {}
  var index = {"index": {"_index": "group7", "_type": "_doc", "_id": `${i}`}};
  var Element = Document[i].split(';')
  for (var j = 0; j < Element.length; j++) {
     Data[Columns[j]] = Element[j]
  }
  Json.push(index)
  Json.push(Data)
}

Data = JSON.stringify(Json)
fs.writeFileSync('group7.json', Data, function (err) {
  if (err) throw err
})
