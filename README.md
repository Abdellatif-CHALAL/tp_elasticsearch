# TP_ElasticSearch

TP sur ElasticSearch

# les commandes afin de créer group7.json

- npm install 
- npm jsonParser.js
- installer jq: sur
 - Mac : "brew install jq"
 - Linux : "apt-get install jq"

# Pour lancer le script, il faut exécuter ces commandes suivants pour générer le NDJson:
- node JsonPaser.js
- cat group7.json | jq -c '.[]' > group7NDJSON.json 

# Pour poster les documents dans elaticsearch avec l'index group7:
- curl -s -H "Content-Type: application/x-ndjson" -XPOST localhost:9200/_bulk --data-binary @group7NDJSON.json