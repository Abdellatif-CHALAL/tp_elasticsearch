var fs = require('file-system');
var Document = fs.readFileSync('data.csv').toString().split('\r\n');
var Columns = Document[0];
//Document.shift()
Columns = Columns.split(';')

var Json = []

for (var i = 1; i < Document.length; i++) {
  var Data = {}
  var Element = Document[i].split(';')
  for (var j = 0; j < Element.length; j++) {
     Data[Columns[j]] = Element[j]
  }
  Json.push(Data)
}

Data = JSON.stringify(Json)
fs.writeFileSync('data.json', Data, function (err) {
  if (err) throw err
})